﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoCoreApp.Migrations
{
    public partial class addIpAdressColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IpAdress",
                table: "TokenModels",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IpAdress",
                table: "TokenModels");
        }
    }
}
