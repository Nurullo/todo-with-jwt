﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ToDoCoreApp
{
    public class FileUploadOperation:IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            operation.Parameters.Clear();
            operation.Parameters.Add(new NonBodyParameter()
            {
                Name = "UploadedFile",
                In = "formData",
                Description = "Choose file",
                Required = false,
                Type = "file"
            });
            operation.Consumes.Add("multipart/form-data");
        }
    }
}
