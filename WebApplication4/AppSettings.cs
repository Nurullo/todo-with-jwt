﻿using Newtonsoft.Json;

namespace TodoCore {

    public class AppSettings {
        // Properties for JWT Token Signature
        public string Audience { get; set; }
        public string ExpireTime { get; set; }
        public string Secret { get; set; }
        public string issuer { get; set; }

        public string RefreshToken { get; set; }
        public string GrantType { get; set; }
        public string ClientId { get; set; }
        // 

    }
}