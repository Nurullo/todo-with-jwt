using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using TodoCore;
using TodoCore.Data;
using ToDoCoreApp.Data;
using ToDoCoreApp.ViewModel;

namespace ToDoCoreApp.Controllers
{

    [Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AppSettings _appSettings;
        private readonly TodoDbContext _db;
        private readonly IHttpContextAccessor _accessor;

        public TokenController(UserManager<ApplicationUser> userManager, IOptions<AppSettings> appSettings, IHttpContextAccessor accessor, TodoDbContext db)
        {
            _userManager = userManager;
            _appSettings = appSettings.Value;
            _accessor = accessor;
            _db = db;
        }
        [Route("Auth")]
        [HttpPost]
        public async Task<IActionResult> Auth([FromForm] TokenRequestModel model)
        {
            
            if (model == null)
            {
                return new StatusCodeResult(500);
            }
            switch (model.GrantType)
            {
                case "password":
                    return await GenerateNewToken(model);
                case "refresh_token":
                    return await RefreshToken(model);
                default:
                    return new UnauthorizedResult();
            }
        }

        private string GetIpAdress()
        {
            string strHostName = Dns.GetHostName();
            string clientIPAddress = Dns.GetHostAddresses(strHostName).GetValue(1).ToString();
            return clientIPAddress;
        }

        private async Task<IActionResult> GenerateNewToken(TokenRequestModel model)
        {

            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                //create refresh token 
                var newRtoken = CreateRefreshToken(_appSettings.ClientId, user.Id);
                // delete existing old refresh token 

                
                var oldRTokens = _db.TokenModels.Where(rt => rt.UserId == user.Id && rt.IpAdress == GetIpAdress());

                if (oldRTokens != null)
                {
                    foreach (var oldrt in oldRTokens)
                    {
                        _db.TokenModels.Remove(oldrt);
                    }
                }

                _db.TokenModels.Add(newRtoken);
                await _db.SaveChangesAsync();
                

                var accessToken = await CreateAcessToken(user, newRtoken.Value);

                return Ok(accessToken);
            }
            ModelState.AddModelError("", "UserName/Password was not found");
            return Unauthorized(new { loginError = "Please check the login Credentials - invalid Username/Password was entered" });

        }

        private async Task<TokenResponseModel> CreateAcessToken(ApplicationUser user, string refreshToken)
        {
            double tokeExpireTime = Convert.ToDouble(_appSettings.ExpireTime);
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.Secret));

            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                new Claim (JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim (JwtRegisteredClaimNames.Jti, Guid.NewGuid ().ToString ()),
                new Claim (ClaimTypes.NameIdentifier, user.Id),
                new Claim ("UserId", user.Id),
                new Claim("IpAdress",GetIpAdress()),
                new Claim ("LoggedOn", DateTime.Now.ToString ()),
                }),

                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature),
                Issuer = _appSettings.issuer,
                Audience = _appSettings.Audience,
                Expires = DateTime.Now.AddMinutes(tokeExpireTime)
            };
            //Generate token 
            var newToken = tokenHandler.CreateToken(tokenDescriptor);
            var encodedToken = tokenHandler.WriteToken(newToken);
            return new TokenResponseModel()
            {
                token = encodedToken,
                expiration = newToken.ValidTo,
                refresh_token = refreshToken,
                username = user.UserName
            };

        }
        private TokenModel CreateRefreshToken(string clientId, string userId)
        {

            return new TokenModel
            {
                ClientId = clientId,
                UserId = userId,
                Value = GenerateUniqueRefreshToken(),
                CreatedDate = DateTime.Now,
                LastModifiedDate = DateTime.Now,
                ExpireTime = DateTime.Now.AddMinutes(3),
                IpAdress = GetIpAdress()
            };
        }
       
        private string GenerateUniqueRefreshToken()
        {
            var randomNumer = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumer);
                return Convert.ToBase64String(randomNumer);
            }
        }

        private async Task<IActionResult> RefreshToken(TokenRequestModel model)
        {
            try
            {
                var result = _db.TokenModels.FirstOrDefault(t => t.ClientId == _appSettings.ClientId && t.Value == model.RefreshToken.ToString());

                if (result == null || model.AccessToken == null)
                {
                    return new UnauthorizedResult();
                }

               var check =  CheckIpAdress(model);
                if (check == false)
                {
                    return new UnauthorizedResult();
                }

                if (result.ExpireTime < DateTime.Now)
                {
                    return new UnauthorizedResult();
                }

                var user = await _userManager.FindByIdAsync(result.UserId);

                if (user == null)
                {
                    return new UnauthorizedResult();
                }

                //generate a new refresh token 
                result.Value = GenerateUniqueRefreshToken();
                result.LastModifiedDate = DateTime.Now;
                _db.TokenModels.Update(result);
                _db.SaveChanges();

                var response = await CreateAcessToken(user, result.Value);
                return Ok(new
                {
                    AuthToken = response
                });
            }
            catch (System.Exception ex)
            {
                    
                return new JsonResult(ex.ToString());
            }
        }
        private bool CheckIpAdress(TokenRequestModel model)
        {
            var handler = new JwtSecurityTokenHandler();
            var stream = handler.ReadJwtToken(model.AccessToken);
            var ip = stream.Claims.First(claim => claim.Type == "IpAdress").Value;

            if (ip != GetIpAdress())
            {
                return false;
            }
            return true;
        }

    }
}