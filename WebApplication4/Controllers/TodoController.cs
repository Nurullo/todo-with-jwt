﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoCore.Data;
using ToDoCoreApp.Data;
using ToDoCoreApp.ViewModel;

namespace ToDoCoreApp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly TodoDbContext _todoDbContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public TodoController(TodoDbContext todoDbContext, IHostingEnvironment hostingEnvironment, UserManager<ApplicationUser> userManager)
        {
            _todoDbContext = todoDbContext;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
        }

        private string GetCurrentUserId()
        {
            var claimsIdentity = this.User.Identity as ClaimsIdentity;
            var userId = claimsIdentity.FindFirst("UserId")?.Value;
            return userId;

        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {

            var path = Path.Combine(_hostingEnvironment.WebRootPath, "Upload");

            IList<GetTodoViewModel> result = null;
            result = await _todoDbContext.Todos.Include(t => t.Attachments).Where(t => t.AssignedUserId == GetCurrentUserId()).Select(s =>
            new GetTodoViewModel()
            {
                Id = s.Id,
                Description = s.Description,
                Deadline = s.Deadline,
                StartDate = s.StartDate,
                Title = s.Title,
                IsCompleted = s.IsCompleted,
                Attachments = s.Attachments.Select(p => new AttachmentViewModel
                {
                    Name = Path.Combine(path, p.Name)
                })  
            }).ToListAsync();

            return Ok(result);
        }

        public object AttachmentViewModel { get; set; }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Create([FromForm] CreateTodoViewModel model)
        {
            IFormFileCollection files = Request.Form.Files;

            var userName = _userManager.GetUserId(HttpContext.User);
            var newUser = _userManager.FindByNameAsync(userName);
            var userId = newUser.Result.Id;

            var attachment = new Attachment();
            var todo = new Todo();
            if (ModelState.IsValid)
            {
                todo = new Todo()
                {
                    StartDate = DateTime.Now,
                    AssignedUserId = userId,
                    Deadline = model.Deadline,
                    IsCompleted = model.IsCompleted,
                    Description = model.Description,
                    Title = model.Title,

                };
                await _todoDbContext.Todos.AddAsync(todo);
                await _todoDbContext.SaveChangesAsync();
            }

            if (files != null)
            {
                var folderName = "Upload";
                var webRoot = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRoot, folderName);
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        string fileName = Guid.NewGuid() + "." + file.ContentType.Split('/').Last();
                        string fullPath = Path.Combine(newPath, fileName);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                            attachment = new Attachment()
                            {
                                TodoId = todo.Id,
                                Name = fileName
                            };
                            await _todoDbContext.Attachments.AddAsync(attachment);
                            await _todoDbContext.SaveChangesAsync();
                        }
                    }

                }
            }

            return this.Content("Success");
        }

        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Update([FromForm] UpdateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _todoDbContext.Todos.FindAsync(model.Id);
                if (result != null)
                {

                    result.Id = model.Id;
                    result.Deadline = model.Deadline;
                    result.IsCompleted = model.IsCompleted;
                    result.Description = model.Description;
                    result.Title = model.Title;
                    result.AssignedUserId = model.AssignedUserId;
                    result.StartDate = model.StartDate;

                    _todoDbContext.Todos.Update(result);
                    _todoDbContext.SaveChanges();
                    return Ok();
                }
            }

            return Ok(ModelState);
        }

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {

            var folderName = "Upload";
            var webRoot = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRoot, folderName);

            var result = await _todoDbContext.Todos.FindAsync(id);
            if (result != null)
            {
                var Attachments = await _todoDbContext.Attachments.Where(t => t.TodoId == id).ToListAsync();

                foreach (var attachment in Attachments)
                {
                    var deletePath = Path.Combine(newPath, attachment.Name);
                    if (System.IO.File.Exists(deletePath))
                    {
                        System.IO.File.Delete(deletePath);
                    }
                }

                _todoDbContext.Todos.Remove(result);
                await _todoDbContext.SaveChangesAsync();
            }

            return Ok("Record Successfully deleted");

        }
    }
}