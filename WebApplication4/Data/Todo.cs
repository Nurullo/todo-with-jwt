﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ToDoCoreApp.Data
{
    public class Todo
    {
        public Todo()
        {
           this.Attachments = new HashSet<Attachment>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? Deadline { get; set; }
        [DefaultValue(false)]
        public bool IsCompleted { get; set; }

        public string AssignedUserId { get; set; }
        [ForeignKey("AssignedUserId")]
        public ApplicationUser ApplicationUser { get; set; }

        public ICollection<Attachment> Attachments { get; set; } 
    }
}
