﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoCoreApp.Data
{
    public class Attachment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TodoId { get; set; }
        public Todo Todo { get; set; }

    }
}
