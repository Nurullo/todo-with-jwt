﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ToDoCoreApp.Data {
    public class ApplicationUser : IdentityUser {
        public virtual ICollection<Todo> Todos { get; set; }
        public virtual List<TokenModel> Tokens { get; set; }

    }

    public class ApplicationRole : IdentityRole {

    }

    public class ApplicationUserRole : IdentityUserRole<string> {

    }

    public class ApplicationUserClaim : IdentityUserClaim<string> {

    }

    public class ApplicationUserLogin : IdentityUserLogin<string> { }

    public class ApplicationRoleClaim : IdentityRoleClaim<string> {

    }

    public class ApplicationUserToken : IdentityUserToken<string> {

    }

}