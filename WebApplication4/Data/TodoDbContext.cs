﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ToDoCoreApp.Data;

namespace TodoCore.Data
{
    public class TodoDbContext : IdentityDbContext<
        ApplicationUser, ApplicationRole, string,
        ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin,
        ApplicationRoleClaim, ApplicationUserToken>
    {
        public TodoDbContext(DbContextOptions options) : base(options)
        {

        }



        public DbSet<Todo> Todos { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<TokenModel> TokenModels { get; set; }
    }
}
