﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ToDoCoreApp.ViewModel
{
    public class CreateTodoViewModel
    {
        
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }

        [DefaultValue(false)]
        public bool IsCompleted  { get; set; }

        
    }
}
