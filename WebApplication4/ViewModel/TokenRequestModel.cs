using System;

namespace ToDoCoreApp.ViewModel
{
    public class TokenRequestModel
    {
        public string GrantType { get; set; } //password or refresh token
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public string Password { get; set; }

    }
}
