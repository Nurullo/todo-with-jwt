﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ToDoCoreApp.ViewModel
{
    public class UpdateViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [DefaultValue(false)]
        public bool IsCompleted { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime StartDate { get; set; }
        public string AssignedUserId { get; set; }
        
        
       
    }
}
