﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ToDoCoreApp.ViewModel
{
    public class TestViewModel
    {
        public string name { get; set; }
        public IFormFile file { get; set; }
    }
}
