﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoCoreApp.Data;
using System.Threading.Tasks;

namespace ToDoCoreApp.ViewModel
{
    public class GetTodoViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? Deadline { get; set; }
        public bool IsCompleted { get; set; }
        public string UserId { get; set; }
        public IEnumerable<AttachmentViewModel> Attachments { get; set; }


    }
}
